<?php
header('Access-Control-Allow-Origin: *');

// DEFINE DATABASE CONNECTION VALUES
define("dbhost","localhost");
define("dbuser","focuslabs_api");
define("dbpass","X9_jfVDrSl7");
define("dbname","focuslabs_api");

// MAKE THE PDO CONNECTION
try { $dbh=new PDO("mysql:host=".dbhost.";dbname=".dbname."","".dbuser."","".dbpass.""); }
catch (PDOException $e) { echo 'Connection failed: ' . $e->getMessage(); }

if ($_REQUEST["action"]=="Submit") {
	// CREATE SYSTEM VARIABLES
	$_POST["tracking"]=mt_rand();
	$_POST["status"]="Pending";
	$_POST["submitted_at"]=date('m-d-Y h:i:s a');
	
	// CHECK FOR EMPTY VARIABLES AND FILL WITH NO IF EMPTY
	if (!isset($_POST["firsttimepurchase"])) {$_POST["firsttimepurchase"]="No";}
	if (!isset($_POST["termsandconditions"])) {$_POST["termsandconditions"]="No";}
	if (!isset($_POST["marketingoffers"])) {$_POST["marketingoffers"]="Yes";}

	// ADJUST FOR APP BOOLEANS
	if ($_POST["firsttimepurchase"] == "true") {$_POST["firsttimepurchase"]="Yes";}
	if ($_POST["termsandconditions"] == "true") {$_POST["termsandconditions"]="Yes";}
	if ($_POST["marketingoffers"] == "true") {$_POST["marketingoffers"]="No";}
			
	// SAVE THE RECEIPT IF ATTACHED
	if (!empty($_FILES["attachreceipt"]["name"])) {$_POST["receipt"]=$_POST["tracking"]."-".$_FILES["attachreceipt"]["name"];move_uploaded_file($_FILES["attachreceipt"]["tmp_name"], "receipt_uploads/".$_POST["tracking"]."-".basename($_FILES["attachreceipt"]["name"]));}
	elseif (!empty($_POST["attachreceipt"])) {$_POST["receipt"]=$_POST["tracking"]."-".$_POST["lastname"];file_put_contents("receipt_uploads/".$_POST["receipt"],base64_decode($_POST["attachreceipt"]));}
	else {$_POST["receipt"]="";}

	$stmt=$dbh->prepare("insert into `rebates` (`tracking`,`firstname`,`lastname`,`address`,`city`,`state`,`zip`,`phone`,`email`,`physiciansname`,`product`,`firsttimepurchase`,`termsandconditions`,`marketingoffers`,`attachreceipt`,`status`,`submitted_at`) values (:tracking,:firstname,:lastname,:address,:city,:state,:zip,:phone,:email,:physiciansname,:product,:firsttimepurchase,:termsandconditions,:marketingoffers,:attachreceipt,:status,:submitted_at)");
	$stmt->bindValue(':tracking', $_POST["tracking"]);
	$stmt->bindValue(':firstname', $_POST["firstname"]);
	$stmt->bindValue(':lastname', $_POST["lastname"]);
	$stmt->bindValue(':address', $_POST["address"]);
	$stmt->bindValue(':city', $_POST["city"]);
	$stmt->bindValue(':state', $_POST["state"]);
	$stmt->bindValue(':zip', $_POST["zip"]);
	$stmt->bindValue(':phone', $_POST["phone"]);
	$stmt->bindValue(':email', $_POST["email"]);
	$stmt->bindValue(':physiciansname', $_POST["physiciansname"]);
	$stmt->bindValue(':product', $_POST["product"]);
	$stmt->bindValue(':firsttimepurchase', $_POST["firsttimepurchase"]);
	$stmt->bindValue(':termsandconditions', $_POST["termsandconditions"]);
	$stmt->bindValue(':marketingoffers', $_POST['marketingoffers']);
	$stmt->bindValue(':attachreceipt', $_POST["receipt"]);
	$stmt->bindValue(':status', $_POST["status"]);
	$stmt->bindValue(':submitted_at', $_POST["submitted_at"]);
	$stmt->execute();

	// RETURN THE RESPONSE
	if ($_POST["product"]=="tozal") {$redirect_url="http://www.tozalrebate.com/status.php";} else {$redirect_url="http://www.freshkoterebate.com/status.php";}
	echo "<form method=\"post\" action=\"$redirect_url\" name=\"frm\">";
	foreach ($_POST as $key=>$value) {echo "<input type=\"hidden\" name=\"".htmlentities($key)."\" value=\"".htmlentities($value)."\">";}
	echo "</form><script language=\"JavaScript\">document.frm.submit();</script>";
		}

// LOOKUP BY TRACKING NUMBER
elseif ($_GET["action"]=="lookup_tracking") {
	$stmt=$dbh->prepare("select * from `rebates` where `tracking`=:tracking limit 1");
	$stmt->bindValue(':tracking', $_GET['tracking']);
	$stmt->execute();
	while ($row = $stmt->fetch()) {echo json_encode($row);}
	}
	
// LOOKUP BY NAME
elseif ($_GET["action"]=="lookup_name") {
	$data=explode(",",$_GET["data"]);
	if (empty($_GET["data"])) {$data=array($_GET["lastname"],$_GET["phone"],urldecode($_GET["email"]));}
	$stmt=$dbh->prepare("select * from `rebates` where `lastname`=:lastname and `phone`=:phone and `email`=:email limit 1");
	$stmt->bindValue(':lastname', $data[0]);
    $stmt->bindValue(':phone', $data[1]);
    $stmt->bindValue(':email', $data[2]);
	$stmt->execute();
	while ($row = $stmt->fetch()) {echo json_encode($row);}
	}
?>